#!/bin/sh
#
# v4l2-settings-restore.sh - restore v4l2 device settings
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.
#
# NOTE: this script is meant to be called by an udev rule.

[ "$DEBUG" = "true" ] && set -x
set -e

SETTINGS_DIR="/tmp"
[ -f /etc/default/v4l2-persistent-settings ] && . /etc/default/v4l2-persistent-settings

SETTINGS_FILE="${SETTINGS_DIR}/${ID_V4L_PRODUCT}.conf"

if [ "$ACTION" = "add" ] && [ -f "$SETTINGS_FILE" ];
then
  while read -r setting;
  do
    CTRL=$(echo "$setting" | tr -d '\n' | cut -d ' ' -f 1)
    VAL=$(echo "$setting" | tr -d '\n' | cut -d ' ' -f 2)
    v4l2-ctl -d "$DEVNAME" --set-ctrl "${CTRL}=${VAL}"
  done < "$SETTINGS_FILE"
fi
