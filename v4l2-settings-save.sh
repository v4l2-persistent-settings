#!/bin/sh
#
# v4l2-settings-save.sh - save v4l2 device settings
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

[ "$DEBUG" = "true" ] && set -x
set -e

[ -e "$1" ] || { echo "usage: $(basename "$0") <videodev>" 1>&2; exit 1; }

DEVNAME="$1"

SETTINGS_DIR="/tmp"
[ -f /etc/default/v4l2-persistent-settings ] && . /etc/default/v4l2-persistent-settings

: "${ID_V4L_PRODUCT:=$(v4l2-ctl -d "$DEVNAME" --info | grep "Card type" | cut -d ':' -f 2 | sed 's/^[[:space:]]//' | tr -d '\n')}"

SETTINGS_FILE="${SETTINGS_DIR}/${ID_V4L_PRODUCT}.conf"

# Save the latest settings.
v4l2-ctl -d "$DEVNAME" -l | sed -n '/^[[:space:]]/s/^[[:space:]]*\([^[:space:]]*\).*value=\([^[:space:]]*\).*$/\1 \2/p' > "$SETTINGS_FILE"

