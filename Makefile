install:
	install -m 755 -d $(DESTDIR)/etc/v4l2-persistent-settings
	install -m 755 v4l2-settings-save.sh $(DESTDIR)/etc/v4l2-persistent-settings/v4l2-settings-save.sh
	install -m 755 v4l2-settings-restore.sh $(DESTDIR)/etc/v4l2-persistent-settings/v4l2-settings-restore.sh
	install -m 644 default.conf $(DESTDIR)/etc/default/v4l2-persistent-settings
	install -m 644 90-v4l2-persistent-settings.rules $(DESTDIR)/lib/udev/rules.d/
	udevadm control --reload

uninstall:
	rm -f $(DESTDIR)/lib/udev/rules.d/90-v4l2-persistent-settings.rules
	udevadm control --reload
	rm -f $(DESTDIR)/etc/default/v4l2-persistent-settings
	rm -f $(DESTDIR)/etc/v4l2-persistent-settings/v4l2-settings-restore.sh
	rm -f $(DESTDIR)/etc/v4l2-persistent-settings/v4l2-settings-save.sh
	rmdir $(DESTDIR)/etc/v4l2-persistent-settings || echo 'Try running "make purge".' 1>&2

purge: uninstall
	rm -rf $(DESTDIR)/etc/v4l2-persistent-settings
