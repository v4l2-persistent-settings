#!/bin/sh

set -x
set -e

DEBUG="true" \
  ID_V4L_PRODUCT="dummy" \
  ./v4l2-settings-save.sh /dev/video0

DEBUG="true" \
  ACTION="add" \
  DEVNAME="/dev/video0" \
  ID_V4L_PRODUCT="dummy" \
  ./v4l2-settings-restore.sh
